<?php
use PredictionBuilder\PredictionBuilder;

//require_once __DIR__ . '/vendor/autoload.php';

class DashboardController extends \BaseController{


    public static function adminDashboard(){
        //get admin view info
        $resgisteredUserNumber = User::count()-1;

        //total inactive user number
        $totalInactiveUserNumber = UserInfo::where('activation',false)->count();

        //total activated user number
        $totalUserNumber = UserInfo::where('activation',true)->count()-1;

        //total results added
        $totalResultNumber = Result::count();

        //total course entry
        $totalCourseNumber = Course::count();

        return View::make('admindashboard')
            ->with([
                'title'             =>      'Dashboard',
                'resgisteredUserNumber' => $resgisteredUserNumber,
                'totalInactiveUserNumber'   =>      $totalInactiveUserNumber,
                'totalUserNumber'   =>      $totalUserNumber,
                'totalResultNumber' =>      $totalResultNumber,
                'totalCourseNumber' =>      $totalCourseNumber
            ]);
    }

    public static function userDashboard(){

        $obj = new DashboardController();

        //cgpa chart
        $chartData = $obj->showcoursecgpa();

        //current cgpa
        $current_cgpa = round($obj->calculateCGPA(),2);

        //total drop credits
        $drop_credits  = $obj->getDropCredits();

        //credits left
        $left_credits = $obj->predictGrade(Auth::user()->id);

        //passed credits
        $passed_credits = $obj->getPassedCredits();

        //latest post
        $latest_post = $obj->getLatestPost();


        return View::make('userdashboard')
            ->with([
                'title'             =>      'Dashboard',
                'current_cgpa'      =>      $current_cgpa,
                'passed_credits'    =>      $passed_credits,
                'left_credits'      =>      $left_credits,
                'drop_credits'      =>      $drop_credits,
                'chartData'         =>      $chartData,
                'latest_post'       =>      $latest_post
            ]);
    }

    public function test(){

      $users = User::lists('id');
      $predictions = [];
      foreach($users as $id){
        $predictions[] = predictGrade($id);
      }
      return $predictions;

    }

    public function predictGrade($user_id)
    {
      try{
   		$user = User::findOrFail($user_id);
   		if($user){
   			$results = Result::where('user_id', $user_id)
                  ->where('grade_point','!=',0)
   								->with('course')
   								->get();
        if(count($results) < 3){
          return 'not enough data';
        }
        $resultsArray = [];
        foreach ($results as $result) {
          if($result->grade_point!=0){
          $resultsArray[] = array(
            'semester'  =>  $result->course->course_semester,
            'grade_point' =>  $result->grade_point,
            'credits' =>  $result->course->course_credit
          );
          }
   		  }

        usort($resultsArray, function($a, $b) {
              return $a['semester'] - $b['semester'];
          });

          //return $resultsArray;
          //try to obtain progressive cgpa
          try{
              $total_credits = 0;
              $TGP = 0;
              $progressiveCGPA = array();
              $resultArray = array();

              foreach($resultsArray as $result){
                  $resultArray[] = $result;
                  $total_credits += $result['credits'];
                  $TGP += $result['grade_point']*$result['credits'];
                  $progressiveCGPA[] = array(
                          $total_credits,(float) $TGP/$total_credits
                  );
              }

          }catch (Exception $ex){
              return 'Error calculating progressive cgpa';
          }

          try{
            $x=160;
            $prediction = new PredictionBuilder($x, $progressiveCGPA);
            $result = $prediction->build();
            if($result->y < 0 || $result->y > 4.0){
              return 'ambiguous prediction';
            }else{
              return $result->y;
            }
          }catch(Exception $ex){
            return 'ambiguous';
          }

   		}else{
        return "not available!";
      }
    }catch(Exception $ex){
      return "not found!";
    }
    }

    private function getLatestPost(){
        $user_batch = Auth::user()->userInfo->batch->batch;
        //find the last created post of that batch
        $latest_post = Posts::where('batch',$user_batch)
                            ->with('post_user')
                            ->get()
                            ->sortByDesc('created_at');
        $latest_post = $latest_post->first();
        if($latest_post == null)
            return null;
        return $latest_post;
    }



    public function showcoursecgpa(){
        //plot course vs CGPA graph
        try{
            $user_id = Auth::user()->id;
            $results = Result::where('user_id',$user_id)
                                ->where('grade_point','!=',0)
                                ->get();
            $taken_courses_id = $results->lists('course_id');
            $taken_courses = Course::whereIn('id',$taken_courses_id)
                                    ->orderBy('course_semester','asc')
                                    ->orderBy('course_number','asc')
                                    ->get();;
            $taken_courses_sorted_id = $taken_courses->lists('id');
            $taken_courses_sorted_number = $taken_courses->lists('course_number');

            $gradesArray = array();
            $resultsArray = array();
            $corresponding_courses = array();

            $i=0;
            //sort results as the $taken_courses__sorted_id
            foreach($taken_courses_sorted_id as $taken_course_sorted_id){
                $temp = Result::where('user_id', $user_id)
                    ->where('course_id',$taken_course_sorted_id)
                    ->where('grade_point','!=',0.00)
                    ->with('Course')
                    ->first();
                if( $temp != null) {
                    $corresponding_courses[] = Course::where('id',$taken_course_sorted_id)->pluck('course_number');
                    $resultsArray[] = $temp;
                    $gradesArray[]  = (float) $temp->grade_point;
                }
                ++$i;
            }

            $progressiveCGPA = $this->getProgressiveCGPA($resultsArray);

            $list = $this->getCourseList();

            if(count($resultsArray)<=0){

                return $chartData = [
                    'courseList'=>null,
                    'cgpa'=>null,
                    'grades'=>null,
                    'lists'=>$list
                ];
            }

            return $chartData = [
                    'courseList'=>$corresponding_courses,
                    'cgpa'=>$progressiveCGPA,
                    'grades'=>$gradesArray,
                    'lists'=>$list
                ];

        }catch(Exception $ex){
            return Log::error($ex);
            return Redirect::back()->with(['error'=>'Error generating CGPA graph']);
        }
    }


    private function getProgressiveCGPA($results){
        try{
            $total_credits = 0;
            $TGP = 0;
            $progressiveCGPA = array();
            $resultArray = array();

            foreach($results as $result){
                $resultArray[] = $result;
                $total_credits += $result->course->course_credit;
                $TGP += $result->grade_point*$result->course->course_credit;
                $progressiveCGPA[] = (float) $TGP/$total_credits;
            }
            return $progressiveCGPA;

        }catch (Exception $ex){
            return Redirect::back()->with('error','Error generating subject vs CGPA graph function');
        }
    }

       private function getCourseList(){
        $userInfo = Auth::user()->userInfo;

        $courseInfo = Course::where(['dept_id'=>$userInfo->dept_id, 'batch_id'=>$userInfo->batch_id])->get();
        $courseList = $courseInfo->sortBy('course_semester')->lists('course_number','id');

        return $courseList;

    }

    public function getDropCredits(){
        try{
                $user_id = Auth::user()->id;
                $taken_courses_id = Result::where('user_id',$user_id)->lists('course_id');
                $drop_courses = Result::where('user_id',$user_id)
                    ->where('grade_point',0.00)
                    ->whereIn('course_id',$taken_courses_id)
                    ->with('Course')
                    ->get(['course_id']);

                $drop_credits = 0;

                foreach ($drop_courses as $course) {
                    $drop_credits += (float) $course->course->course_credit;
                }

                return $drop_credits;

            }catch(Exception $ex){
                return Redirect::back()->with(['error'=>'could not count CGPA']);
            }
    }

    public function getLeftCredits(){
        try{
                $user = Auth::user();
                $offered_courses = Course::where('dept_id',$user->userInfo->dept_id)
                                            ->where('batch_id',$user->userInfo->batch_id)
                                            ->lists('course_credit');
                $offered_credits = 0;

                foreach ($offered_courses as $course) {
                    $offered_credits += (float) $course;
                }

                $passed_credits = $this->getPassedCredits();

                $credits_left = $offered_credits - $passed_credits;

                return $credits_left;

            }catch(Exception $ex){
                return Redirect::back()->with(['error'=>'could not count CGPA']);
            }
    }

    public function getPassedCredits(){
        try{
                $user_id = Auth::user()->id;
                $taken_courses_id = Result::where('user_id',$user_id)->lists('course_id');
                $passed_courses = Result::where('user_id',$user_id)
                    ->where('grade_point','!=',0.00)
                    ->whereIn('course_id',$taken_courses_id)
                    ->with('Course')
                    ->get(['course_id']);

                $passed_credits = 0;

                foreach ($passed_courses as $course) {
                    $passed_credits += (float) $course->course->course_credit;
                }

                return $passed_credits;

            }catch(Exception $ex){
                return Redirect::back()->with(['error'=>'could not count CGPA']);
            }
    }

    public function calculateCGPA(){
            try{
                $user_id = Auth::user()->id;
                $taken_courses_id = Result::where('user_id',$user_id)->lists('course_id');
                $results = Result::where('user_id',$user_id)
                    ->where('grade_point','!=',0.00)
                    ->whereIn('course_id',$taken_courses_id)
                    ->with('Course')
                    ->get();
                $CGPA = $this->calculateTotalCGPA($results);

                if($CGPA==0){
                    return 'not enough data to count your CGPA';
                }else
                    return $CGPA;

            }catch(Exception $ex){
                return Redirect::back()->with(['error'=>'could not count CGPA']);
            }
        }
    private function calculateTotalCGPA($results){
        try{
            $total_credits = 0;
            $TGP = 0;
            foreach($results as $result){
                $total_credits += $result->course->course_credit;
                $TGP += $result->grade_point*$result->course->course_credit;
            }
            $cgpa = $TGP/$total_credits;
            return $cgpa;

        }catch (Exception $ex){

        }
    }
}
?>
