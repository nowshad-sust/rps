@extends('layouts.default')
@section('content')
        @include('includes.statMenu')
        @include('includes.alert')
    <div class="panel-body">
    <h2>Result Sheet</h2>
    @if($course)
    <h3>{{$course->course_number}} - {{ $course->course_title}}</h3>
    @endif
    <div class="col-md-7">
      @if($results)
        <table class="display table table-bordered table-stripe text-center" id="example">
            <thead>
            <tr>
                <th>Registration number</th>
                <th>Grade Letter</th>

            </tr>
            </thead>
            <tbody>
            @foreach($results as $Info)
                @if($Info['grade_point'] == 0)
                    <tr class="danger">
                @elseif($Info['grade_point'] == 4)
                    <tr class="success">
                 @else
                    <tr class="">
                @endif
                    <td>{{$Info['reg_no']}}</td>
                    <td>{{$Info['grade_letter']}}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
        @endif
      </div>
      <div class="col-md-5">
        <h3>select a course to see the marksheet</h3>
        <ul>
          @foreach($courseList as $id => $course)
            <li><a href="{{route('resultSheet',$id)}}">{{$course}}</a></li>
          @endforeach
        </ul>
      </div>
    </div>



@stop

@section('style')
    {{ HTML::style('assets/data-tables/DT_bootstrap.css') }}

@stop


@section('script')
    <!--
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {

            $('#example').dataTable({
            });
        });
    </script>
    -->
@stop
