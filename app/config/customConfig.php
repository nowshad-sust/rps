<?php

return [
	'names' =>[
		'siteName' => 'SUSTian RPS',
		'siteTitle'	=>	[
			'first'	=>	'SUSTian',
			'last'	=>	'RPS',
			'version'=>	'Beta'
		]
	],
	'roles' =>[
		'admin' => 'admin',
		'user' => 'user',
		'manager' => 'manager'
	]
];
